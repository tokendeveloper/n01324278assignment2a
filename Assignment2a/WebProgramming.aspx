﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2a.WebForm2" %>

<asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="jumbo" class="jumbotron">
        <h1>Web Programming</h1>
        <p>Let's get studying for your mid-terms!</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Concept" runat="server">
    <h2>For Loop</h2>
    <p>
        For Loop allows you to repeat a task over and over, until it fulfills its condition.
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodeSnippet" runat="server">
    <pre>
        <code>
            for(i= 0; i < 5; i++){

                //Block of code for the logic you are trying to repeat.

            }
        </code>
    </pre>
</asp:Content>
<asp:Content ContentPlaceHolderID = "ExplainCode" runat="server">
    <h2>How it works</h2>
    <p>
        "for loop" is initiated with the keyword "for", followed by "( )", which will include the loop conditions,
        then followed by "{ }", where the logic of what you want to repeat goes. 
    </p>
    <p>
        "i" in this case is a variable. It doesn't have to be "i". It can be "j", "k", anything. 
    </p>
    <p>
        The first condition (i.e. "i = 0") sets the start position of the loop.
        In this case, 0 is the start position, meaning the "first item". 
    </p>
    <p>
        The second portion, "i < 5" defines the number of times it will loop. In this case, the 
        loop with repeat the logic 5 times. 
    </p>
    <p>
        The final condition, "i++" is a incrementor condition. In this case, i++ is the shorthand for writing "i = i + 1", or "i += 1". This means that 
        the logic will be executed everytime the block of code runs, until the condition is met (in this case, until the loop runs through 5 times). 
        If the condition was "i += 2" instead, the loop will run every other time the block of code is executed (in this case, 3 times, when i = 0, i = 2, and i = 4).
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="ExampleCode" runat="server">
   <h2>Example</h2>
   <pre>
        <code>
            //Taken from https://www.w3schools.com/js/tryit.asp?filename=tryjs_loop_for_ex

            var text = "";
            var i;

            for (i = 0; i < 5; i++) {
                text += "The number is " + i;
            }

            console.log(text);

            //This will log the following to the console:
            //The number is 0
            //The number is 1
            //The number is 2
            //The number is 3
            //The number is 4
        </code>
    </pre>
    <hr />

</asp:Content>

<asp:Content ContentPlaceHolderID="UsefulLinks" runat="server">
    <h2>List of Useful Links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/js/js_loop_for.asp" target="_blank">W3 Schools -JavaScript For Loop</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration" target="_blank">Mozilla Developer Network -Loops and Iteration</a></li>
    </ul>
</asp:Content>
