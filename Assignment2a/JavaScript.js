﻿var darkButton = document.getElementById('toggleDark')
var body = document.getElementById('body');
var jumbotron = document.getElementById('jumbo')

//client click
// cookie created which says it's dark.
//if there is a cookie, stay dark.
//if there is a cookie and the client clicks back to normal, clear cookie.

window.onload = displayModeOnLoad();

function displayModeOnLoad() {
    if (document.cookie === 'dark') {
        toggleDark();
    } else if (document.cookie === 'not dark') {
        toggleNormal();
    }
}

//Reference from https://www.w3schools.com/js/js_cookies.asp
//The function sets a cookie, which allows for the display mode (ie dark vs normal) to persist throughout page refreshes. 
//Ohhh my goodness. It took me a good 10 hours to figure out how to make client-side state to persist through page-refreshes. 

function toggleDisplayMode(){
    if (body.className === 'dark') {
        toggleNormal();
    } else if (body.className === '') {
        toggleDark();
    }
}

function toggleNormal() {
    document.cookie = 'not dark';
    body.className = '';
    jumbotron.className = 'jumbotron';
    darkButton.innerHTML = 'Switch to Dark Mode';
}

function toggleDark() {
    document.cookie = 'dark';
    body.className += 'dark';
    jumbotron.className += ' dark';
    darkButton.innerHTML = 'Back to Normal'; 
}