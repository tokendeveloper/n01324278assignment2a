﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebAppDev.aspx.cs" Inherits="Assignment2a.WebForm4" %>
<asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="jumbo" class="jumbotron">
        <h1>Web Application Development</h1>
        <p>Let's get studying for your mid-terms!</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Concept" runat="server">
    <h2>Classes</h2>
    <p>Classes are essentially a blueprint for objects. For example, if you want to create a "student" object, you could create a student class, and add
        variables (or fields) and functions (or methods) that you might want for a student object within a class. You might have a student ID, student name,
        student GPA fields to construct a student class. An object is an instance of a class.
    </p>

</asp:Content>

<asp:Content ContentPlaceHolderID="CodeSnippet" runat="server">
    <pre>
        <code>
            public class Student 
            {   
                //These are fields (variables within a class).
                public string ID;
                public string Name;
                private double GPA;
                
                //This is a constructor method.
                public Student(string id, string name)
                {
                    ID = id;
                    Name = name;
                }
                
                //This is a data-accessor
                public string GPA
                {
                    //These are property accessors for a private field.
                    get { return GPA; }
                    set { GPA = value; }
                }

            }
        </code>
    </pre>
</asp:Content>

<asp:Content ContentPlaceHolderID = "ExplainCode" runat="server">
    <h2>Explaining the code above</h2>
    <p>A class in C# is instantiated by the key words "class" + *your class name (Student, in this case)*. A class can be accessible as private, or public. For this example, the student class is 
        public.
    </p>
    <p>
        The next block of code defines the fields, or the variables that belong to the student class. ID and Name are public, while the GPA is private. 
    </p>
    <p>
        The block of code right below the fields is a constructor method, and it is named the same way as the class name. The constructor method is the method 
        that you would call in order to construct the object. The set of brackets next to the method name 
        defines the parameters of the method, which are the publically accessible fields (ID and Name in this case). It's important to remember that the parameters also have defined
        datatypes that are consistent with the fields that you are inserting into the method. 
    </p>
    <p>
        For the private field, GPA, we use a data-accessor, defined by the "get" and "set" keywords, instead of the constructor method. 
    </p>
    <p>
        An object can be instantiated by first calling the class, followed by a variable name of your choice (we'll use the variable name, "newStudent"). 
        You can then assign an instance of the class by using the "new" keyword, followed by the constructor method. 
        The parameters of the constructor method will take the values of the variables that you defined, which should hold the values that you want in the object
        (in this case, the parameter "id" should take the variable holding the value of a student ID, and "name" should hold the value of the student's Name). 
        You now have an object called "newStudent", constructed from the Student class. The properties of the object can be retrieved through the dot notation.
        <pre>
            <code>
                string var1 = "007";
                string var2 = "James Bond"
                double var3 = 4.00;

                Student newStudent = new Student(var1, var2)
                newStudent.ID = var1;
                newStudent.Name = var2;
                newStudent.GPA = var3;
           </code>
        </pre>

    </p>
</asp:Content>

<asp:Content ContentPlaceHolderID="ExampleCode" runat="server">
   <h2>Example</h2>
   <pre>
        <code>
            //Taken from https://stackoverflow.com/questions/1130199/methods-and-constructors

            public class MyType
            {
                private SomeType _myNeeds;

                // constructor
                MyType(SomeType iWillNeedThis)
                {
                    _myNeeds = iWillNeedThis;
                }

                // method
                public void MyMethod()
                {
                    DoSomethingAbout(_myNeeds);
                }
            }
        </code>
    </pre>
    <hr />

</asp:Content>

<asp:Content ContentPlaceHolderID="UsefulLinks" runat="server">
    <h2>List of Useful Links</h2>
    <ul>
        <li><a href="https://stackoverflow.com/questions/1130199/methods-and-constructors" target="_blank">Stack Overflow -Methods and Constructors</a></li>
        <li><a href="https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/classes" target="_blank">Microsoft -C# Programming Guide</a></li>
    </ul>
</asp:Content>