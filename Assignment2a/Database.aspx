﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment2a.WebForm3" %>
<asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="jumbo" class="jumbotron">
        <h1>Database Design</h1>
        <p>Let's get studying for your mid-terms!</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Concept" runat="server">
    <h2>Constraint</h2>
    <p>Constraints are essentially rules that restrict the type of data that can be used in a table column.</p>

</asp:Content>

<asp:Content ContentPlaceHolderID="CodeSnippet" runat="server">
    <pre>
        <code>
            --Here are the types of constraints in SQL:

            PRIMARY KEY

            FOREIGN KEY

            NOT NULL

            DEFAULT 

            UNIQUE

            CHECK

        </code>
    </pre>
</asp:Content>

<asp:Content ContentPlaceHolderID = "ExplainCode" runat="server">
    <h2>What do each of the constraints do?</h2>
    <p>
        <strong>PRIMARY KEY:</strong> This constraint takes a unique value that cannot be replicated within the same column. An ID would be a good primary key. 
    </p>
    <p>
        <strong>Foreign Key:</strong> This constraint references a primary key from another table. 
    </p>    
    <p>
        <strong>NOT NULL:</strong> This constraint does not allow for the value of to be null. 
    </p>
    <p>
        <strong>DEFAULT:</strong> This constraint assigns a specific value to a row by default, unless otherwise specified.
    </p>   
    <p>
        <strong>UNIQUE:</strong> This constraint the data belonging to a column to be distinct and non-repeatable.
    </p>
    <p>    
        <strong>CHECK:</strong> CHECK ensures that the data assigned satisfies specified conditions.
    </p>
</asp:Content>

<asp:Content ContentPlaceHolderID="ExampleCode" runat="server">
   <h2>Example</h2>
   <pre>
        <code>
            --Here is how you set up constraints in Oracle SQL
            --Taken from https://www.w3schools.com/sql/sql_constraints.asp

           CREATE TABLE table_name (
                column1 datatype PRIMARY KEY,
                column2 datatype FOREIGN KEY,
                column3 datatype NOT NULL
           );
        </code>
    </pre>
    <hr />

</asp:Content>

<asp:Content ContentPlaceHolderID="UsefulLinks" runat="server">
    <h2>List of Useful Links</h2>
    <ul>
        <li><a href="https://www.tutorialspoint.com/sql/sql-constraints.htm" target="_blank">Tutorials Point -SQL Constraints</a></li>
        <li><a href="https://www.w3schools.com/sql/sql_constraints.asp" target="_blank">W3 Schools -SQL Constraints</a></li>
    </ul>
</asp:Content>