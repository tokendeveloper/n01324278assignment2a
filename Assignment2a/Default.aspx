﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a.WebForm1" %>

<asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    <div id="jumbo" class="jumbotron">
        <h1>Kento's Study Guide.</h1>
        <p>Let's get studying for your mid-terms!</p>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Concept" runat="server">

    <div class="row">
        <div class="default-list col-lg-4">
            <a runat="server" href="~/WebProgramming"><h2>Web Programming</h2></a>
            <p>Let's go over the basics of loops. Let's go over the basics of loops. Let's go over the basics of loops.</p>
        </div>
        <div class="default-list col-lg-4">
             <a runat="server" href="~/Database"><h2>Database Design</h2></a>
            <p>What the heck is a constraint? It's pretty important, buddy. Gotta do some review here.</p>
        </div>
        <div class="default-list col-lg-4">
            <a runat="server" href="~/WebAppDev"><h2>WebApp Development</h2></a>
            <p>Mastering the syntax for creating classes in C# can be difficult. Let's review this!</p>
        </div>
    </div>

</asp:Content>

<asp:Content ContentPlaceHolderID="TextSection" runat="server">
    <div class="row encouragement">
        <div class="col-md-6">
            <h2>Words of Encouragement</h2>
                <p>
                    "Do it. <br />
                    Just do it.<br />
                </p>
             
                <p>
                    Don't let your dreams be dreams.<br />
                    Yesterday you said tomorrow.<br />
                    So just do it.<br />
                    Make your dreams come true.<br />
                    Just do it.<br />
                </p>
               
                <p>
                    Some people dream of success<br />
                    While you're gonna wake up and work hard at it.<br />
                    Nothing is impossible.<br />
                </p>

                <p>
                    You should get to the point<br />
                    Where anyone else would quit<br />
                    And you're not going to stop there.<br />
                    No, what are you waiting for?<br />
                </p>

                <p>
                    Do it<br />
                    Just do it.<br />
                    Yes you can.<br />
                    Just do it.<br />
                    If you're tired of starting over<br />
                    Stop giving up."<br />
                </p>
              
                <em>-Shia LaBeouf</em>
        </div>
        <div class="col-md-6">
            <iframe width="500" height="315" src="https://www.youtube.com/embed/ZXsQAXx_ao0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>

    </div>
    
</asp:Content>
